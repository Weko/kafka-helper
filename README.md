# Kafka helper

## Settings
- KAFKA_TOPICS - list of topics which should listen handler
- KAFKA_GROUP - consumer group name
- KAFKA_SERVERS - list of ip addresses of kafka servers
- KAFKA_TASKS_MODULES - list of project modules with handler tasks
